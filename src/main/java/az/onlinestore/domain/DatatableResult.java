package az.onlinestore.domain;

import java.io.Serializable;

public class DatatableResult implements Serializable {
    private static final long serialVersionUID = 8711431435919586218L;
    private long draw;
    private long totaldata;
    private long filterdata;
    private Object[][] data;

    public long getDraw() {
        return draw;
    }

    public void setDraw(long draw) {
        this.draw = draw;
    }

    public long getTotaldata() {
        return totaldata;
    }

    public void setTotaldata(long totaldata) {
        this.totaldata = totaldata;
    }

    public long getFilterdata() {
        return filterdata;
    }

    public void setFilterdata(long filterdata) {
        this.filterdata = filterdata;
    }

    public Object[][] getData() {
        return data;
    }

    public void setData(Object[][] data) {
        this.data = data;
    }
}

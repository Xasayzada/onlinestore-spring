package az.onlinestore.domain;

import java.io.Serializable;
import java.time.LocalDateTime;

public class Token implements Serializable {


    private static final long serialVersionUID = 1576870318950856628L;
    private long id;
    private String value;
    private LocalDateTime generationDate;
    private  LocalDateTime expireDate;
    private User user;
    private boolean used;
    private TokenType tokenType;
    private LocalDateTime insertDate;
    private LocalDateTime lastUpDate;

    public Token() {
        this.id=0;
        this.value="";
        this.user = new User();
    }

    public TokenType getTokenType() {
        return tokenType;
    }

    public void setTokenType(TokenType tokenType) {
        this.tokenType = tokenType;
    }

    public boolean isUsed() {
        return used;
    }

    public void setUsed(boolean used) {
        this.used = used;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public LocalDateTime getGenerationDate() {
        return generationDate;
    }

    public void setGenerationDate(LocalDateTime generationDate) {
        this.generationDate = generationDate;
    }

    public LocalDateTime getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(LocalDateTime expireDate) {
        this.expireDate = expireDate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public LocalDateTime getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(LocalDateTime insertDate) {
        this.insertDate = insertDate;
    }

    public LocalDateTime getLastUpDate() {
        return lastUpDate;
    }

    public void setLastUpDate(LocalDateTime lastUpDate) {
        this.lastUpDate = lastUpDate;
    }

    @Override
    public String toString() {
        return "Token{" +
                "id=" + id +
                ", value='" + value + '\'' +
                ", generationDate=" + generationDate +
                ", expireDate=" + expireDate +
                ", user=" + user +
                ", used=" + used +
                ", insertDate=" + insertDate +
                ", lastUpDate=" + lastUpDate +
                ", tokentype = " + tokenType +
                '}';
    }
}

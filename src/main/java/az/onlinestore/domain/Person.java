package az.onlinestore.domain;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class Person implements Serializable {

    private static final long serialVersionUID = -5404978743471984617L;
    private long id;
    private String name;
    private String surname;
    private LocalDate birthdate;
    private LocalDateTime registrationdate;

    public Person(long id, String name, String surname, LocalDate birthdate, LocalDateTime registrationdate) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.birthdate = birthdate;
        this.registrationdate = registrationdate;
    }

    public Person() {
        this.id=0;
        this.name="";
        this.surname="";
        this.birthdate =null;
        this.registrationdate =null;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public LocalDate getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(LocalDate birthdate) {
        this.birthdate = birthdate;
    }

    public LocalDateTime getRegistrationdate() {
        return registrationdate;
    }

    public void setRegistrationdate(LocalDateTime registrationdate) {
        this.registrationdate = registrationdate;
    }
}

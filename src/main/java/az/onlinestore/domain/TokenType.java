package az.onlinestore.domain;

import java.util.Arrays;

public enum TokenType {
    ACTIVATION(1), RESET_PASSWORD(2);

    private int value;

    TokenType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static TokenType fromValue(long value){
        return Arrays.stream(values())
                .filter(tokenType -> tokenType.value==value)
                .findFirst().orElseThrow(() ->new RuntimeException("Invalid token type"));
    }


}

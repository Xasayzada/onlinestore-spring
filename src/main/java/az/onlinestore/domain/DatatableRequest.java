package az.onlinestore.domain;

public class DatatableRequest {
    private int draw;
    private int start;
    private int length;
    private int sortcolumn;
    private String sortdir;
    private String filter;

    public int getDraw() {
        return draw;
    }

    public void setDraw(int draw) {
        this.draw = draw;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getSortcolumn() {
        return sortcolumn;
    }

    public void setSortcolumn(int sortcolumn) {
        this.sortcolumn = sortcolumn;
    }

    public String getSortdir() {
        return sortdir;
    }

    public void setSortdir(String sortdir) {
        this.sortdir = sortdir;
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    @Override
    public String toString() {
        return "DatatableRequest{" +
                "draw=" + draw +
                ", start=" + start +
                ", length=" + length +
                ", sortcolumn=" + sortcolumn +
                ", sortdir='" + sortdir + '\'' +
                ", filter='" + filter + '\'' +
                '}';
    }
}

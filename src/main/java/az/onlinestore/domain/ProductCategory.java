package az.onlinestore.domain;

import java.io.Serializable;

public class ProductCategory implements Serializable {
    private long id;
    private String name;
    private long count;
    private String iconClass;

    public ProductCategory(long id, String name, long count, String iconClass) {
        this.id = id;
        this.name = name;
        this.count = count;
        this.iconClass = iconClass;
    }

    public ProductCategory(long id, String name, long count) {
        this.id = id;
        this.name = name;
        this.count = count;
    }

    public ProductCategory() {
        this.id = 0;
        this.name = "";
        this.count = 0;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public String getIconClass() {
        return iconClass;
    }

    public void setIconClass(String iconClass) {
        this.iconClass = iconClass;
    }

    @Override
    public String toString() {
        return "ProductCategory{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", count=" + count +
                ", iconClass='" + iconClass + '\'' +
                '}';
    }
}

package az.onlinestore.domain;

import java.util.Arrays;

public enum UserStatus {
        PENDING(1), ACTIVE(2), DEACTIVE(3), LOCKED(4) ;

    private int value;

    UserStatus(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static UserStatus fromvalue(long value){
        UserStatus type = null;

        type = Arrays.stream(values()).filter(userStatus -> userStatus.getValue() == value)
                .findFirst().orElseThrow(() -> new RuntimeException("Invalid user status " + value));

        return type;
    }
}

package az.onlinestore.domain;

import java.util.Arrays;

public enum Role {

    ADMIN(1), CUSTOMER(2);

    private long value;

    Role(long value) {
        this.value = value;
    }

    public static Role fromvalue(long value){
        Role role = null;

        role = Arrays.stream(values()).filter(userrole -> userrole.getValue() == value)
                .findFirst().orElseThrow(() -> new RuntimeException("Invalid user role " + value));

        return role;
    }

    public long getValue() {
        return value;
    }
}

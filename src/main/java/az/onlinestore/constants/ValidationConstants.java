package az.onlinestore.constants;

public class ValidationConstants {

    public static final int FIRST_NAME_MIN_LENGTH = 2;
    public static final int FIRST_NAME_MAX_LENGTH = 50;
    public static final int last_name_min_length = 2;
    public static final int last_name_max_length = 45;
    public static final int password_max_length = 45;
    public static final int password_min_length = 6;
    public static final int EMAIL_MAX_LENGTH = 50;
    public static final int PASSWORD_MIN_LENGTH = 6;
    public static final int PASSWORD_MAX_LENGTH = 50;

}

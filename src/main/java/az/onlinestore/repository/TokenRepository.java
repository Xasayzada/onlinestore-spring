package az.onlinestore.repository;

import az.onlinestore.domain.Token;

import java.util.Optional;

public interface TokenRepository {
    Token addToken(Token token);
    Optional<Token> getToken(String token);
    void updateToken(String token);
}

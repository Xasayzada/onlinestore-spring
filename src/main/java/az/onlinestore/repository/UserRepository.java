package az.onlinestore.repository;

import az.onlinestore.domain.Role;
import az.onlinestore.domain.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository {
    User addUser(User user);
    void addUserRole(long userId, Role role);
    Optional<User> getUserByEmail(String email);
    List<Role> getUserRole(long userId);
}

package az.onlinestore.repository.jdbc;

import az.onlinestore.domain.Product;
import az.onlinestore.domain.ProductCategory;
import az.onlinestore.repository.ProductRepository;
import az.onlinestore.repository.RowMapper.ProductRowMapper;
import az.onlinestore.repository.RowMapper.SubtypesRowMapper;
import az.onlinestore.repository.SqlQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public class ProductRepositoryJdbcImpl implements ProductRepository {
    @Autowired
    private NamedParameterJdbcTemplate namedJdbcTemplate;

    @Autowired
    private ProductRowMapper productRowMapper;

    @Autowired
    private SubtypesRowMapper subtypesRowMapper;

    @Override
    public List<ProductCategory> getProductCategory() {
        return namedJdbcTemplate.getJdbcTemplate().query(SqlQuery.GET_PRODUCT_COUNT_BY_TYPE, (resultSet, i) ->
                new ProductCategory(resultSet.getLong("id"),
                        resultSet.getString("type"),
                        resultSet.getLong("product_count"),
                        resultSet.getString("icon_class")));
    }

    @Override
    public List<Product> getProductByType(String type) {
        SqlParameterSource params = new MapSqlParameterSource().addValue("type", type);
        return namedJdbcTemplate.query(SqlQuery.GET_PRODUCT_BY_TYPE, params, productRowMapper);
    }

    @Override
    public List<Product> getAllProduct(long limit) {
        SqlParameterSource params = new MapSqlParameterSource().addValue("limit", limit);
        return namedJdbcTemplate.query(SqlQuery.GET_ALL_PRODUCTS,params,  productRowMapper);
    }

    @Override
    public List<Map<String, String>> getSubtypes() {
        return namedJdbcTemplate.getJdbcTemplate().query(SqlQuery.GET_SUBTYPES, subtypesRowMapper);

    }
}

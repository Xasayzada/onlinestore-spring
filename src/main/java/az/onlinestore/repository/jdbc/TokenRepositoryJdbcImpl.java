package az.onlinestore.repository.jdbc;

import az.onlinestore.domain.Token;
import az.onlinestore.repository.RowMapper.TokenRowMapper;
import az.onlinestore.repository.SqlQuery;
import az.onlinestore.repository.TokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class TokenRepositoryJdbcImpl implements TokenRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    private TokenRowMapper tokenRowMapper;

    @Override
    public Token addToken(Token token) {
        Object[] args = {token.getValue(),
                token.getGenerationDate(),
                token.getExpireDate(),
                token.getUser().getId(),
                token.getTokenType().getValue()};
        int count = jdbcTemplate.update(SqlQuery.ADD_TOKEN, args);

        if (count > 0) {
            //todo add log
            System.out.println("token adding success");
        } else {
            throw new RuntimeException("Error adding token");
        }
        return token;
    }

    @Override
    public Optional<Token> getToken(String token) {
        Optional<Token> optionalToken = Optional.empty();

        Object[] args = {token};
        Token tokenObj = jdbcTemplate.queryForObject(SqlQuery.GET_TOKEN, args, tokenRowMapper);
        optionalToken = Optional.of(tokenObj);

        return optionalToken;
    }

    @Override
    public void updateToken(String token) {
        //todo mark token as used
        System.out.println("token marked");
    }
}

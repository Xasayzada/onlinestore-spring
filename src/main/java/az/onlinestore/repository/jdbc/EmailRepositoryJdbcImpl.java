package az.onlinestore.repository.jdbc;

import az.onlinestore.domain.Email;
import az.onlinestore.repository.EmailRepository;
import org.springframework.stereotype.Repository;

@Repository
public class EmailRepositoryJdbcImpl implements EmailRepository {
    @Override
    public Email addEmail(Email email) {
        return email;
    }
}

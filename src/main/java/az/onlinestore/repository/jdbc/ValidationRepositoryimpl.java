package az.onlinestore.repository.jdbc;

import az.onlinestore.repository.SqlQuery;
import az.onlinestore.repository.ValidationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

@Repository
public class ValidationRepositoryimpl implements ValidationRepository {

    @Autowired
    NamedParameterJdbcTemplate jdbcTemplate;

    @Override
    public boolean isusedmail(String email) {
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("email", email);
        int count = jdbcTemplate.queryForObject(SqlQuery.CHECK_EMAIL,params, Integer.class);

        return count>0;
    }
}

package az.onlinestore.repository.jdbc;

import az.onlinestore.domain.Role;
import az.onlinestore.domain.User;
import az.onlinestore.repository.RowMapper.RoleRowMapper;
import az.onlinestore.repository.RowMapper.UserRowMapper;
import az.onlinestore.repository.SqlQuery;
import az.onlinestore.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;
import java.util.Optional;

@Repository
public class UserRepositoryJdbcImpl implements UserRepository {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private NamedParameterJdbcTemplate namedJdbcTemplate;

    @Autowired
    private UserRowMapper userRowMapper;

    @Autowired
    private RoleRowMapper roleRowMapper;

    @Override
    public User addUser(User user) {
//        Object[] args = {};
//        int count = jdbcTemplate.update(SqlQuery.ADD_USER,args);

        KeyHolder keyHolder = new GeneratedKeyHolder();
        int count = jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection.prepareStatement(SqlQuery.ADD_USER, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, user.getName());
            ps.setString(2, user.getSurname());
            ps.setString(3, user.getEmail());
            ps.setString(4, user.getPassword());
            return ps;
        }, keyHolder);

        if (count > 0) {
            user.setId(keyHolder.getKey().longValue());
        } else {
            //todo custom exception
            throw new RuntimeException("Error adding user " + user.getName());
        }

        return user;
    }

    @Override
    public void addUserRole(long userId, Role role) {

//        "insert into user_role (id, user_id, role_id ) " +
//            " values(null, :user_id, :role_id )
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("user_id", userId)
                .addValue("role_id", role.getValue());

        int count = namedJdbcTemplate.update(SqlQuery.ADD_USER_ROLE, params);
        if (count == 1) {
            //todo log
            System.out.println("succes adding role");
        } else {
            throw new RuntimeException("Error adding uer role");
        }
    }

    @Override
    public Optional<User> getUserByEmail(String email) {
        Optional<User> optionalUser = Optional.empty();
        SqlParameterSource params = new MapSqlParameterSource().addValue("email", email);
        List<User> userList = namedJdbcTemplate.query(SqlQuery.GET_USER_BY_EMAIL, params, userRowMapper);
        if (userList.get(0) != null) {
            optionalUser = Optional.of(userList.get(0));
        }
        return optionalUser;
    }

    @Override
    public List<Role> getUserRole(long userId) {
        SqlParameterSource params= new MapSqlParameterSource().addValue("userid", userId);
        List<Role> roleList = namedJdbcTemplate.query(SqlQuery.GET_USER_ROLES, params,roleRowMapper);
        return roleList;
    }

    public List<Role> getUserRoles(long userId) {
        SqlParameterSource params = new MapSqlParameterSource().addValue("userId", userId);
//        List<Role>  roleList= namedJdbcTemplate.query(SqlQuery.GET_USER_ROLE , params);

        return null;
    }


}

package az.onlinestore.repository.RowMapper;

import az.onlinestore.domain.Token;
import az.onlinestore.domain.TokenType;
import az.onlinestore.domain.UserStatus;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class TokenRowMapper implements RowMapper<Token> {

    @Override
    public Token mapRow(ResultSet resultSet, int i) throws SQLException {

        Token tokenObj = new Token();
        tokenObj.setValue(resultSet.getString("value"));
        tokenObj.setUsed(resultSet.getLong("used")==1);
        tokenObj.setTokenType(TokenType.fromValue(resultSet.getLong("token_type_id")));
        tokenObj.getUser().setId(resultSet.getLong("user_id"));
            tokenObj.getUser().setPassword(resultSet.getString("password"));

        tokenObj.getUser().setName(resultSet.getString("name"));
        tokenObj.getUser().setSurname(resultSet.getString("surname"));
        tokenObj.getUser().setEmail(resultSet.getString("email"));
        tokenObj.getUser().setUserStatus(UserStatus.fromvalue(resultSet.getLong("user_status_id")));
        tokenObj.getUser().setIdate(resultSet.getTimestamp("idate").toLocalDateTime());
        if (resultSet.getTimestamp("udate")!=null) {
            tokenObj.getUser().setUdate(resultSet.getTimestamp("udate").toLocalDateTime());
        }
        tokenObj.setGenerationDate(resultSet.getTimestamp("generation_date").toLocalDateTime());
        tokenObj.setExpireDate(resultSet.getTimestamp("expire_date").toLocalDateTime());
        tokenObj.setInsertDate(resultSet.getTimestamp("idate").toLocalDateTime());
        if (resultSet.getTimestamp("udate")!=null){
            tokenObj.setInsertDate(resultSet.getTimestamp("udate").toLocalDateTime());
        }
        return tokenObj;
    }
}

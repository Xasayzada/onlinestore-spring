package az.onlinestore.repository.RowMapper;

import az.onlinestore.domain.User;
import az.onlinestore.domain.UserStatus;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class UserRowMapper implements RowMapper<User> {
    @Override
    public User mapRow(ResultSet resultSet, int i) throws SQLException {
        User user= new User();
        user.setId(resultSet.getLong("id"));
        user.setName(resultSet.getString("name"));
        user.setSurname(resultSet.getString("surname"));
        user.setPassword(resultSet.getString("password"));
        user.setUserStatus(UserStatus.fromvalue(resultSet.getLong("user_status_id")));
        user.setEmail(resultSet.getString("email"));

        return user;
    }
}

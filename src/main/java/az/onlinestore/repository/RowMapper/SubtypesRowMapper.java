package az.onlinestore.repository.RowMapper;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@Component
public class SubtypesRowMapper implements RowMapper<Map<String, String>> {
    @Override
    public Map<String, String> mapRow(ResultSet resultSet, int i) throws SQLException {
        Map<String, String> subTypesMap = new HashMap<>();
        subTypesMap.put(resultSet.getString("type"), resultSet.getString("subtype"));
        return subTypesMap;
    }
}

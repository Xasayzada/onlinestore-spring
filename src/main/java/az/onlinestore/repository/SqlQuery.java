package az.onlinestore.repository;

public class SqlQuery {

    public static final String ADD_USER = "insert into user (id, name ,surname,  user_status_id, email, password ) " +
            " values(null, ?, ?, 1, ?,? )";

    public static final String ADD_USER_ROLE = "insert into user_role (id, user_id, role_id ) " +
            " values(null, :user_id, :role_id )";

    public static final String ADD_TOKEN ="insert into token (id, value, generation_date, expire_date, user_id, token_type_id)  " +
            " values (null, ?, ?,?,?,?)";

    public static  final String GET_TOKEN =   " select  token.value,token.udate,token.generation_date, token.expire_date,token.used," +
            " token.token_type_id, token.idate, token.user_id, user.name, user.surname, user.email, user.idate, user.udate, " +
            " user.password ,   user.user_status_id   from token " +
            " join user on token.user_id = user.id " +
            "where token.value = ? and token.status=1";

    public static  final String CHECK_EMAIL = "select count(id) cnt from user  " +
            "   where email = :email and status = 1";

    public static  final String GET_USER_BY_EMAIL = " select id, name ,  surname, password, email, user_status_id " +
            " from user where email = :email and  status=1 ";

    public static final  String GET_USER_ROLES = " select role.id " +
            " from user_role join role on user_role.role_id= role.id and  user_role.status=1  and role.status=1 " +
            " where user_id =:userid ";


    public static final String GET_PRODUCT_COUNT_BY_TYPE = "select p.id , p.type ,p.icon_class, count(product.id) product_count " +
            " from product " +
            "join product_type on  product_type.id=product.type_id and product.status=1  " +
            "join product_type p on product_type.parent_id = p.id and p.status=1    " +
            "group by  p.id ,p.type " +
            "order by product_count desc "  ;

    public static final String GET_PRODUCT_BY_TYPE = "select p.id, par.type, p.name, p.price from product p " +
            "join product_type pt on pt.id = p.type_id  " +
            "join  product_type par on par.id= pt.parent_id " +
            "where par.type = :type" +
            "  limit :limit,  12 ";

    public static final String GET_ALL_PRODUCTS = "select p.id, par.type, p.name, p.price from product p " +
            " join product_type pt on pt.id = p.type_id " +
            " join  product_type par on par.id= pt.parent_id " +
            " limit :limit,  12 ";

    public static final String GET_SUBTYPES = "select  p.type subtype , pt.type type from  "+
            " product_type p  join product_type pt on pt.id= p.parent_id  ";


}

package az.onlinestore.repository;

import az.onlinestore.domain.Email;

public interface EmailRepository {
    Email addEmail(Email email);
}

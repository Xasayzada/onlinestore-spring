package az.onlinestore.repository;

import az.onlinestore.domain.Product;
import az.onlinestore.domain.ProductCategory;

import java.util.List;
import java.util.Map;

public interface ProductRepository {
    List<ProductCategory> getProductCategory();
    List<Product> getProductByType(String type);
    List<Product> getAllProduct(long limit);
    List<Map<String, String>> getSubtypes();
}

package az.onlinestore.interceptor;

import az.onlinestore.domain.Role;
import az.onlinestore.domain.User;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URI;

@Component
public class SecurityInterceptor extends HandlerInterceptorAdapter {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        boolean success = false;

        if (request.getSession().getAttribute("user") != null) {
            User user = (User) request.getSession().getAttribute("user");
            URI uri = new URI(request.getRequestURL().toString());
            System.out.println("request.getRequestURL().toString()  = " + request.getRequestURL().toString());
            String url = getNormalizedUrl(uri, request);
            System.out.println("NormalizedUrl = " + url);
            String requestUrl = request.getRequestURL().toString();

            if (requestUrl.startsWith(url + "admin") &&
                    user.getRoleList().stream().anyMatch(role -> role == Role.ADMIN)) {
                success = true;

            } else if (requestUrl.startsWith(url + "customer") &&
                    user.getRoleList().stream().anyMatch(role -> role == Role.CUSTOMER)) {
                success = true;
            } else {
                System.out.println("unknown sit");
            }
        } else {
            response.sendRedirect(request.getContextPath() + "/login");
        }
        return success;
    }


    private static String getNormalizedUrl(URI uri, HttpServletRequest request) {
        String url = "";
        if (uri.getPort() != -1) {
            url = uri.getScheme() + "://" + uri.getHost() + ":" + uri.getPort() + "/" + request.getContextPath();
            System.out.println("uri.getScheme() = " + uri.getScheme());
            System.out.println("uri.getHost() = " + uri.getHost());
            System.out.println("uri.getPort() = " + uri.getPort());
            System.out.println("request.getContextPath() = " + request.getContextPath());
        } else {
            url = uri.getScheme() + "://" + uri.getHost() + "/" + request.getContextPath();
            System.out.println("uri.getScheme() = " + uri.getScheme());
            System.out.println("uri.getHost() = " + uri.getHost());
            System.out.println("uri.getPort() = " + uri.getPort());
            System.out.println("request.getContextPath() = " + request.getContextPath());
        }
        return url;
    }
}

package az.onlinestore.controller;

import az.onlinestore.domain.*;
import az.onlinestore.service.ProductService;
import az.onlinestore.service.TokenService;
import az.onlinestore.service.UserService;
import az.onlinestore.service.ValidationService;
import az.onlinestore.util.FormUtility;
import az.onlinestore.validator.UserRegistrationFormValidator;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@RequestMapping("/")
@Controller
public class WebController {
    @Autowired
    private UserRegistrationFormValidator candidateRegistrationFormValidator;

    @Autowired
    private UserService userService;

    @Autowired
    private TokenService tokenService;

    @Autowired
    ValidationService validationService;

    @Autowired
    private ProductService productService;

    @InitBinder
    public void dataBind(WebDataBinder dataBinder) {
        if (dataBinder.getTarget() != null && dataBinder.getTarget().getClass().equals(UserRegistrationForm.class)) {
            dataBinder.setValidator(candidateRegistrationFormValidator);
        }
    }

    @PostMapping("/register_customer")
    public ModelAndView registerCandidate(@ModelAttribute("userRegistrationForm") @Validated UserRegistrationForm form,
                                          BindingResult validationResult) {
        ModelAndView modelAndView = new ModelAndView();
        System.out.println("form = " + form);
        if (validationResult.hasErrors()) {
            modelAndView.setViewName("web/register");
        } else {
            try {
                User user = FormUtility.getUser(form);
                user = userService.registerUser(user);
                modelAndView.setViewName("web/register-success");
            } catch (Exception e) {
                e.printStackTrace();
                modelAndView.setViewName("web/register-error");
            }
        }
        return modelAndView;
    }

    @GetMapping("/")
    public ModelAndView index() {
        ModelAndView modelAndView= new ModelAndView("web/index");
        List<ProductCategory> categoryList = productService.getProductCategory();
        modelAndView.addObject("categoryList", categoryList);
        modelAndView.addObject("typeMap", productService.getSubtypes());
        return modelAndView;
    }

    @GetMapping("/login")
    public String login() {
        return "web/login";
    }

    @PostMapping("/login")
    public ModelAndView login(HttpSession session) {
        ModelAndView modelAndView = new ModelAndView();
        session.setAttribute("user", new User());
        return modelAndView;
    }

    @GetMapping("/register")
    public ModelAndView register() {
        ModelAndView modelAndView = new ModelAndView("web/register");
        UserRegistrationForm form = new UserRegistrationForm();
        modelAndView.addObject("userRegistrationForm", form);
        return modelAndView;
    }

    @GetMapping("/activate")
    public ModelAndView activate(@RequestParam(name = "token") String token) {
        ModelAndView modelAndView = new ModelAndView();
        String errorMessage = "";
        if (token != null && !token.equals("")) {
            Optional<Token> optionalToken = tokenService.getToken(token);
            if (optionalToken.isPresent()) {
                Token tokenObj = optionalToken.get();
                if (tokenObj.getExpireDate().isAfter(LocalDateTime.now())) {
                    User user = tokenObj.getUser();
                    if (!tokenObj.isUsed()) {

                        if (user.getUserStatus() == UserStatus.PENDING) {
                            // set userstatus active
//                        userService.activateUser(user.getId());
                            tokenService.updateToken(token);
                            //mark as used
                        } else {
                            tokenService.updateToken(token);
                            // mark token as used
                            // log paranormal activity
                        }
                    } else {
                        errorMessage = "Bu token artıq istifadə olunub";
                    }
                } else {
                    errorMessage = "Tokenin istifadə müddəti bitib";
                }
            } else {
                errorMessage = "Token mövcud deyil";
            }
        } else {
            errorMessage = "Yanlış token";
        }

        if (!errorMessage.isEmpty()) {
            modelAndView.addObject("errorMessage", errorMessage);
            modelAndView.setViewName("web/token-error");
        } else {
            modelAndView.setViewName("activation-success");
        }

        return modelAndView;
    }

    @GetMapping("/check_email")
    @ResponseBody
    public boolean checkEmail(@RequestParam(name = "email") String email) {
        return validationService.isusedmail(email);
    }

    @PostMapping("candidate_signin")
    public ModelAndView candidateSignin(@RequestParam(name = "signin_email") String email,
                                        @RequestParam(name = "signin_password") String password
                                        , HttpSession session) {
        ModelAndView modelAndView = new ModelAndView();
        Optional<User> optionalUser = userService.getUserByEmail(email);
        User user = optionalUser.get();
        String errorMessage = "";
        if (optionalUser.isPresent() && BCrypt.checkpw(password, user.getPassword())) {
            if (user.getUserStatus() == UserStatus.ACTIVE) {
                System.out.println("login succes");
                List<Role> roleList = userService.getUserRole(user.getId());
                if (!roleList.isEmpty()) {
                    user.setRoleList(roleList);
                    session.setAttribute("user",user);

                    String page = "";

                    System.out.println(roleList);

                    if (roleList.stream().anyMatch(role -> role == Role.ADMIN)) {
                        page = "/admin/";
                    } else if (roleList.stream().anyMatch(role -> role == Role.CUSTOMER)) {
                        page = "/customer/";
                    }

                    modelAndView.setViewName("redirect:" + page);
                }

            } else if (user.getUserStatus() == UserStatus.PENDING) {
                errorMessage = "Zəhmət olmasa profilinizi aktivləşdirin";
            } else if (user.getUserStatus() == UserStatus.LOCKED) {
                errorMessage = "Profiliniz bloklanb";
            } else {
                errorMessage = "Profiliniz silinib ";
            }

        } else {
            errorMessage = "Email və ya şifrə yanlışdır";
        }
        if (!errorMessage.isEmpty()) {
            modelAndView.addObject("errormessage", errorMessage);
            modelAndView.setViewName("web/login");
        }
        return modelAndView;
    }

//    @GetMapping("/products")
//    public ModelAndView product(){
//        ModelAndView modelAndView= new ModelAndView();
//        modelAndView.setViewName("web/shop");
//        return modelAndView;
//    }

    @GetMapping("/products")
    public ModelAndView product(@RequestParam(name = "type") String type,
                                @RequestParam(name = "limit") long limit){
        ModelAndView modelAndView= new ModelAndView("web/shop");
        List<ProductCategory> categoryList = productService.getProductCategory();
        modelAndView.addObject("categoryList", categoryList);
        if (type.equals("all")){
            List<Product> productList = productService.getAllProduct(limit);
            modelAndView.addObject("productList", productList);
            System.out.println(productList);
        }else {
            List<Product> productList = productService.getProductByType(type);
            modelAndView.addObject("productList", productList);
        }
        modelAndView.addObject("typeMap", productService.getSubtypes());
        System.out.println(type);
        return modelAndView;
    }

    @GetMapping("/sebet")
    public ModelAndView sebet(){
        ModelAndView modelAndView = new ModelAndView("web/sebet");
        return modelAndView;
    }





}


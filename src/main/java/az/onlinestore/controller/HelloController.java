package az.onlinestore.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;

@RequestMapping("/hello")
@Controller
public class HelloController {

    @GetMapping("/")
    public String index() {
        return "hello";
    }

    @GetMapping("/test")
    public String test() {
        return "tofiq-test";
    }

    @PostMapping("/test")
    public ModelAndView processform(
            @RequestParam(name = "ad") String ad,
            @RequestParam(name = "soyad") String soyad,
            @RequestParam (name = "maas") BigDecimal maas
    ) {
        ModelAndView modelAndView = new ModelAndView("tofiq-test");
        modelAndView.addObject("ad",ad);
        modelAndView.addObject("soyad",soyad);
        modelAndView.addObject("maas",maas);
        return modelAndView;
    }
}

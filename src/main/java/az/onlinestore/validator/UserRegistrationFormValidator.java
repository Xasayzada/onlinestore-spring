package az.onlinestore.validator;

import az.onlinestore.domain.UserRegistrationForm;
import org.apache.commons.validator.GenericValidator;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import static az.onlinestore.constants.ValidationConstants.*;


@Component
public class UserRegistrationFormValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.equals(UserRegistrationForm.class);
    }

    @Override
    public void validate(Object o, Errors errors) {
        UserRegistrationForm form = (UserRegistrationForm) o;
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "firstname", "candidate.registration.firstname.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lastname", "candidate.registration.lastname.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "candidate.registration.email.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "candidate.registration.password.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "passwordconfirmation",
                "candidate.registration.passwordconfirmation.required");

        if (!errors.hasErrors()) {
            //firstname
            if (form.getFirstname().length() < FIRST_NAME_MIN_LENGTH) {
                errors.rejectValue("firstname", "candidate.registration.firstname.minlength");
            } else if (form.getFirstname().length() > FIRST_NAME_MAX_LENGTH) {
                errors.rejectValue("firstname", "candidate.registration.firstname.maxlength");
            }

            //lastname
            if (form.getLastname().length() < last_name_min_length) {
                errors.rejectValue("lastname", "candidate.registration.lastname.minlength");
            } else if (form.getLastname().length() > last_name_max_length) {
                errors.rejectValue("lastname", "candidate.registration.latname.maxlength");
            }

            //email
            if (!GenericValidator.isEmail(form.getEmail())) {
                errors.rejectValue("email", "candidate.registration.email.invalid");
            } else {
                System.out.println();
                //todo isused email
            }

            //password & passwordconfirm
            if (form.getPassword().length() < password_min_length) {
                errors.rejectValue("password", "candidate.registration.password.minlength");
            } else if (form.getPassword().length() > password_max_length) {
                errors.rejectValue("password", "candidate.registration.password.maxlength");
            } else if (!form.getPasswordconfirmation().equals(form.getPassword())){
                errors.rejectValue("passwordconfirmation", "candidate.registration.passwordconfirmation.equal");
            }
        }

    }
}

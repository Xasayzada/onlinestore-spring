package az.onlinestore.service.Impl;

import az.onlinestore.domain.Product;
import az.onlinestore.domain.ProductCategory;
import az.onlinestore.repository.ProductRepository;
import az.onlinestore.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class ProductServiceImpl implements ProductService {
    @Autowired
    private ProductRepository productRepository;
    @Override
    public List<ProductCategory> getProductCategory() {
        return productRepository.getProductCategory();
    }

    @Override
    public List<Product> getProductByType(String type) {
        return productRepository.getProductByType(type);
    }

    @Override
    public List<Product> getAllProduct(long limit) {
        return productRepository.getAllProduct(limit);
    }

    @Override
    public List<Map<String, String>> getSubtypes() {
        return productRepository.getSubtypes();
    }
}

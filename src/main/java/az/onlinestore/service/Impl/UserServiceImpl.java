package az.onlinestore.service.Impl;

import az.onlinestore.domain.*;
import az.onlinestore.repository.UserRepository;
import az.onlinestore.service.EmailService;
import az.onlinestore.service.PasswordService;
import az.onlinestore.service.TokenService;
import az.onlinestore.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private PasswordService passwordService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TokenService tokenService;

    @Autowired
    private EmailService emailService;

    @Transactional
    @Override
    public User registerUser(User user) {

        String hashed = passwordService.encodePassword(user.getPassword());
        user.setPassword(hashed);

        User userrep = userRepository.addUser(user);

        userRepository.addUserRole(userrep.getId(), Role.CUSTOMER);

        Token token = tokenService.generateToken(user);
        token.setTokenType(TokenType.ACTIVATION);

        token = tokenService.addToken(token);

        Email activationEmail = emailService.generateActivationEmail(token);

        emailService.addEmail(activationEmail);

        return user;
    }

    @Override
    public Optional<User> getUserByEmail(String email) {
        return userRepository.getUserByEmail(email);
    }

    @Override
    public List<Role> getUserRole(long userId) {
        return userRepository.getUserRole(userId);
    }
}

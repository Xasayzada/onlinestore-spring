package az.onlinestore.service.Impl;

import az.onlinestore.domain.Token;
import az.onlinestore.domain.User;
import az.onlinestore.repository.TokenRepository;
import az.onlinestore.service.TokenService;
import az.onlinestore.util.CommonUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
public class TokenServiceImpl implements TokenService {

    @Autowired
    private TokenRepository tokenRepository;

    @Override
    public Token generateToken(User user) {
        Token token = new Token();

        token.setValue(CommonUtility.generateToken());
        token.setUser(user);
        token.setGenerationDate(LocalDateTime.now());
        token.setExpireDate(LocalDateTime.now().plusDays(3));

        return token;
    }

    @Override
    public Token addToken(Token token) {
        return tokenRepository.addToken(token);
    }

    @Override
    public Optional<Token> getToken(String token) {
        return tokenRepository.getToken(token);
    }

    @Override
    public void updateToken(String token) {
        tokenRepository.updateToken(token);
    }
}

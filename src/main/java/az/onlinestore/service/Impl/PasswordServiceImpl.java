package az.onlinestore.service.Impl;

import az.onlinestore.service.PasswordService;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.stereotype.Service;

@Service
public class PasswordServiceImpl implements PasswordService {
    @Override
    public String encodePassword(String clearPassword) {
        return BCrypt.hashpw(clearPassword,BCrypt.gensalt());
    }
}

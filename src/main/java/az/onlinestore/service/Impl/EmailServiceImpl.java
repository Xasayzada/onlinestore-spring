package az.onlinestore.service.Impl;

import az.onlinestore.domain.Email;
import az.onlinestore.domain.Token;
import az.onlinestore.service.EmailService;
import az.onlinestore.util.EmailTemplate;
import org.springframework.stereotype.Service;

@Service
public class EmailServiceImpl implements EmailService {


    @Override
    public Email generateActivationEmail(Token token) {
        Email email = new Email();

        String link = "http://employee.az/activate?token=" + token.getValue();
        String body = EmailTemplate.registrationEmail
                (token.getUser().getName(), token.getUser().getSurname(), link);
        email.setSubject(EmailTemplate.registrationSubject());
        email.setFrom("employee.az");
        email.setTo(token.getUser().getName() + " " + token.getUser().getSurname());
        email.setBody(body);
        //todo move to config
        System.out.println("activation email = " + email);

        return email;
    }

    @Override
    public Email addEmail(Email email) {
        return null;
    }
}

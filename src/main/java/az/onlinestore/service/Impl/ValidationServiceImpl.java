package az.onlinestore.service.Impl;

import az.onlinestore.repository.ValidationRepository;
import az.onlinestore.service.ValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class ValidationServiceImpl implements ValidationService {
    @Autowired
    private ValidationRepository validationRepository;

    @Override
    public boolean isusedmail(String email) {
        return validationRepository.isusedmail(email);
    }
}



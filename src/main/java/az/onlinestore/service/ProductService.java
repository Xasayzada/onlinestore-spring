package az.onlinestore.service;

import az.onlinestore.domain.Product;
import az.onlinestore.domain.ProductCategory;

import java.util.List;
import java.util.Map;

public interface ProductService {
    List<ProductCategory> getProductCategory();
    List<Product> getProductByType(String type);
    List<Product> getAllProduct(long limit);
    List<Map<String, String>> getSubtypes();
}

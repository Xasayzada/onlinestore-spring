package az.onlinestore.service;

import az.onlinestore.domain.Token;
import az.onlinestore.domain.User;

import java.util.Optional;

public interface TokenService {
    Token generateToken(User user);
    Token addToken(Token token);
    Optional<Token> getToken(String token);
    void updateToken(String token);

}

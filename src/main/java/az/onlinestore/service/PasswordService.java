package az.onlinestore.service;

public interface PasswordService {
    String encodePassword(String clearPassword);
}

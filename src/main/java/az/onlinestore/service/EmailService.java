package az.onlinestore.service;

import az.onlinestore.domain.Email;
import az.onlinestore.domain.Token;

public interface EmailService {
    Email generateActivationEmail(Token token);
    Email addEmail(Email email);
}

package az.onlinestore.service;

import az.onlinestore.domain.Role;
import az.onlinestore.domain.User;

import java.util.List;
import java.util.Optional;

public interface UserService {
    User registerUser(User user);

    Optional<User> getUserByEmail(String email);

    List<Role> getUserRole(long userId);
}

package az.onlinestore.util;

public class EmailTemplate {
    public static String registrationSubject(){
        return "employee.az registration";
    }
    public static  String registrationEmail(String name, String surname, String link){
        return  String.format("Hörmətli %s %s , " +
                "Employee.az saytında qeydiyyatdan keçdiyiniz üçün təşəkkür edirik." +
                "Profilinizi aktivləşdirmək üçün %s linkə daxil olun." +
                "Hörmətlə, " +
                "Employee.az" , name, surname, link) ;
    }
}

package az.onlinestore.util;

import az.onlinestore.domain.User;
import az.onlinestore.domain.UserRegistrationForm;

public class FormUtility {
    public static  User getUser(UserRegistrationForm form){
        User user =  new User();
        user.setName(form.getFirstname());
        user.setSurname(form.getLastname());
        user.setEmail(form.getEmail());
        user.setPassword(form.getPassword());

        return user;
    }
}

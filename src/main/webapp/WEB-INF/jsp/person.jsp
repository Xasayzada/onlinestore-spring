<%--
  Created by IntelliJ IDEA.
  User: student
  Date: 09.10.19
  Time: 21:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Person Profile</title>
</head>
<body>
    <c:choose>
        <c:when test="${not empty requestScope.person}">
            ID: ${requestScope.person.id} <br/>
            Name: ${requestScope.person.name} <br/>
            Surname: ${requestScope.person.surname} <br/>
            Birth date: ${requestScope.person.birthDate} <br/>
            Registration date: ${requestScope.person.registrationDate} <br/>
        </c:when>
        <c:otherwise>
            Person not found!
        </c:otherwise>
    </c:choose>
</body>
</html>

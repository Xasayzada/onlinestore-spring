<%--
  Created by IntelliJ IDEA.
  User: student
  Date: 09.10.19
  Time: 21:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Person Management</title>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css"/>
</head>
<body>
    <c:choose>
        <c:when test="${not empty requestScope.personList}">
            <h1>Person List</h1>
            <table id="person-table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Surname</th>
                        <th>Birth Date</th>
                        <th>Registration Date</th>
                        <th>Action</th>
                    </tr>
                </thead>

                <tbody>
                    <c:forEach items="${requestScope.personList}" var="person">
                    <tr>
                        <td>${person.id}</td>
                        <td>${person.name}</td>
                        <td>${person.surname}</td>
                        <td>${person.birthDate}</td>
                        <td>${person.registrationDate}</td>
                        <td>
                            <a href="person?action=view&id=${person.id}">View</a> &nbsp;
                            <a href="person?action=edit&id=${person.id}">Edit</a> &nbsp;
                            <a href="person?action=delete&id=${person.id}">Delete</a> &nbsp;
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </c:when>
        <c:otherwise>
            No data found!
        </c:otherwise>
    </c:choose>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function() {
        $('#person-table').DataTable({
            "lengthMenu": [[5, 10, 50, -1], [5, 10, 50, "Hamisi"]],
            "processing": true,
            "serverSide": true,
            "ajax": "person?action=getPersonListAjax"
        });
    } );
</script>
</body>
</html>

<%--
  Created by IntelliJ IDEA.
  User: Reshad
  Date: 11/21/2019
  Time: 9:53 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="py-1 bg-primary">
    <div class="container">
        <div class="row no-gutters d-flex align-items-start align-items-center px-md-0">
            <div class="col-lg-12 d-block">
                <div class="row d-flex">
                    <div class="col-md pr-4 d-flex topper align-items-center">
                        <div class="icon mr-2 d-flex justify-content-center align-items-center"><span
                                class="icon-phone2"></span></div>
                        <span class="text">+99450544897</span>
                    </div>
                    <!--  <div class="col-md pr-4 d-flex topper align-items-center">
                          <div class="icon mr-2 d-flex justify-content-center align-items-center"><span
                                  class="icon-paper-plane"></span></div>
                          <span class="text">qayalimarket@email.com</span>
                      </div>-->

                </div>
            </div>
        </div>
    </div>
</div>
<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
    <div class="container">
        <a class="navbar-brand" href="index.jsp">qayalı market</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav"
                aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="oi oi-menu"></span> Menu
        </button>

        <div class="collapse navbar-collapse" id="ftco-nav">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active"><a href="index.jsp" class="nav-link">Ana səhifə</a></li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="dropdown04" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false">Alış-veriş</a>
                    <div class="dropdown-menu" aria-labelledby="dropdown04">
                        <a class="dropdown-item" href="products?type=all&limit=0">Məhsullar</a>
                        <a class="dropdown-item" href="wishlist.jsp">Almaq istədiklərim</a>
                        <a class="dropdown-item" href="passtoerder.jsp">Sifariş ver</a>
                    </div>
                </li>
                <li class="nav-item"><a href="about.jsp" class="nav-link">Haqqımızda</a></li>
                <li class="nav-item"><a href="contact.jsp" class="nav-link">Əlaqə</a></li>
                <li class="nav-item"><a href="blog.jsp" class="nav-link">Blog</a></li>
                <li class="nav-item cta cta-colored"><a href="sebet" class="nav-link">Səbətim <span
                        class="icon-shopping_cart"></span>[0]</a></li>

            </ul>
        </div>
    </div>
</nav>
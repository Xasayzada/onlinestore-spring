<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 22-Oct-19
  Time: 07:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=utf-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <title>Register</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800&display=swap"
          rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Amatic+SC:400,700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="css/animate.css">

    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">

    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/ionicons.min.css">

    <link rel="stylesheet" href="css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="css/jquery.timepicker.css">


    <link rel="stylesheet" href="css/flaticon.css">
    <link rel="stylesheet" href="css/icomoon.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/stylelogin.css">
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro|Open+Sans+Condensed:300|Raleway"
          type="text/css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="css/passwordscheck.css"/>

    <style>
        .jerror {
            color: white;
            display: none;
            font-size: 15px;
        }
        .error {
            color: white;
            font-size: 15px;
        }
    </style>
</head>
<body class="goto-here">
<div class="py-1 bg-primary">
    <div class="container">
        <div class="row no-gutters d-flex align-items-start align-items-center px-md-0">
            <div class="col-lg-12 d-block">
                <div class="row d-flex">
                    <div class="col-md pr-4 d-flex topper align-items-center">
                        <div class="icon mr-2 d-flex justify-content-center align-items-center"><span
                                class="icon-phone2"></span></div>
                        <span class="text">+99450544897</span>
                    </div>
                    <div class="col-md pr-4 d-flex topper align-items-center">
                        <div class="icon mr-2 d-flex justify-content-center align-items-center"><span
                                class="icon-paper-plane"></span></div>
                        <span class="text">qayalimarket@email.com</span>
                    </div>
                    <div class="col-md-5 pr-4 d-flex topper align-items-center text-lg-right">
                        <span class="text">1 gün ərzində geri qaytarıla bilər</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
    <div class="container">
        <a class="navbar-brand" href="index.html">qayalı market</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav"
                aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="oi oi-menu"></span> Menu
        </button>

        <div class="collapse navbar-collapse" id="ftco-nav">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active"><a href="index.html" class="nav-link">Ana səhifə</a></li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="dropdown04" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false">Alış-veriş</a>
                    <div class="dropdown-menu" aria-labelledby="dropdown04">
                        <a class="dropdown-item" href="shop.html">Məhsullar</a>
                        <a class="dropdown-item" href="wishlist.html">Almaq istədiklərim</a>
                        <a class="dropdown-item" href="product-single.html">Məhsul</a>
                        <a class="dropdown-item" href="cart.html">Səbətim</a>
                        <a class="dropdown-item" href="checkout.html">Sifariş ver</a>
                    </div>
                </li>
                <li class="nav-item"><a href="about.html" class="nav-link">Haqqımızda</a></li>
                <li class="nav-item"><a href="contact.html" class="nav-link">Əlaqə</a></li>
                <li class="nav-item"><a href="giris.html" class="nav-link">Giriş</a></li>
                <li class="nav-item cta cta-colored"><a href="cart.html" class="nav-link">Səbətim <span
                        class="icon-shopping_cart"></span>[0]</a></li>

            </ul>
        </div>
    </div>
</nav>
<!-- END nav -->
<div class="box">


    <div class="login-page">
        <div class="form">

            <form:form modelAttribute="userRegistrationForm"  class="login-form" action="register_customer" method="post">
                <p class="message"></p>

                <span id="first_name_error" class="jerror"></span>
                <form:input path="firstname" id="first_name"    placeholder="ad"/>
                <form:errors path="firstname" cssClass="error"/>


                <span id="last_name_error" class="jerror"></span>
                <form:input path="lastname"  id="last_name" name="last_name"  placeholder="soyad"/>
                <form:errors path="lastname" cssClass="error"/>

                <span id="email_error" class="jerror"></span>
                <form:input path="email"   id="email"  placeholder="email"/>
                <form:errors path="email" cssClass="error"/>

                <span id="password_error" class="jerror"></span>
                <form:password path="password"  id="password" name="password"  placeholder="parol"/>
                <form:errors path="password" cssClass="error"/>
                <span id="result"></span>

                <form:password path="passwordconfirmation"  id="password_confirm" name="password_confirm"
                placeholder="təkrar parol"/>
                <form:errors path="passwordconfirmation" cssClass="error"/>
                <span id="passwordconfirm_error" class="jerror"></span>

                <input type="submit" value="Qeydiyyat" class="btn btn-primary login_btn">
                <p class="message">Qeydiyyatdan keçmisiniz?</br> <a href="login"
                                                                    class="btn btn-outline-danger btn-sm active"
                                                                    type="button">Daxil ol</a></p>
            </form:form>

        </div>
    </div>

</div>

<script src='https://code.jquery.com/jquery-3.4.1.min.js'></script>
<script>
    $('.message a').click(function () {
        $('form').animate({height: "toggle", opacity: "toggle"}, "slow");
    })
</script>

<script src="js/jquery.min.js"></script>
<script src="js/jquery-migrate-3.0.1.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/jquery.waypoints.min.js"></script>
<script src="js/jquery.stellar.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.magnific-popup.min.js"></script>
<script src="js/aos.js"></script>
<script src="js/jquery.animateNumber.min.js"></script>
<script src="js/bootstrap-datepicker.js"></script>
<script src="js/scrollax.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
<script src="js/google-map.js"></script>
<script src="js/main.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script src="js/passwordscheck.js"></script>
<script>
    $(document).ready(function() {
        /*  function getBaseUrl() {
              var re = new RegExp(/^.*\//);
              return re.exec(window.location.href);
          }*/
        function isEmail(email) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(email);
        }

        function validateAlphaFirst(){
            var textInput = document.getElementById("first_name").value;
            textInput = textInput.replace(/[^A-Za-z]/g, "");
            document.getElementById("first_name").value = textInput;
        }

        function validateAlphaLast(){
            var textInput = document.getElementById("last_name").value;
            textInput = textInput.replace(/[^A-Za-z]/g, "");
            document.getElementById("last_name").value = textInput;
        }



        function validateFirstName(){
            var valid = true;
            var value=$('#first_name').val();

            if(value.length < 2 ){
                valid=false;
                $('#first_name_error').text('Ad 2 simvoldan az ola bilməz');
                $('#first_name_error').show();
            }else if(value.length > 45){
                valid=false;
                $('#first_name_error').text('Ad 45 simvoldan çox ola bilməz');
                $('#first_name_error').show();
            }
            if(valid){
                $('#first_name_error').text('');
                $('#first_name_error').hide();
            }
            return valid;
        }

        function validateLastName(){
            var valid = true;
            var value=$('#last_name').val();
            if(value.length < 2 ){
                valid=false;
                $('#last_name_error').text('Soyad 2 simvoldan az ola bilməz');
                $('#last_name_error').show();
            }else if(value.length > 45){
                valid=false;
                $('#first_name_error').text('Soyad 45 simvoldan çox ola bilməz');
                $('#first_name_error').show();
            }
            if(valid){
                $('#last_name_error').text('');
                $('#last_name_error').hide();
            }
            return valid;
        }

        function getBaseUrl() {
            var re = new RegExp(/^.*\//);
            return re.exec(window.location.href);
        }

        function validateEmail(){
            var email=$('#email').val();
            var valid = true;
            var errorMessage ='';

            if(isEmail(email)){
                if(email.length > 45){
                    valid=false;
                    errorMessage ='Email 45 simvoldan çox ola bilməz';
                }else{

                    $.ajax({
                        url: getBaseUrl() +'/check_email',
                        data: {
                            'email': email
                        },
                        method: 'GET',
                        success: function (responseFromServer) {
                            console.log('server response =' + responseFromServer);
                            if(responseFromServer){
                                valid=false;
                                errorMessage ='Bu email istifadə olunub';
                                $('#email_error').text(errorMessage);
                                $('#email_error').show();
                            }
                        }

                    });
                }
            } else if(email!='') {
                valid=false;
                errorMessage='Email düzgün deyil';
            }
            if(valid){
                $('#email_error').text('');
                $('#email_error').hide();
            } else{
                $('#email_error').text(errorMessage);
                $('#email_error').show();
            }
            return valid;
        }

        function validatePassword(){
            var valid = true;
            var value=$('#password').val();

            if (value.length < 6 ){
                valid=false;
                $('#password_error').text('Parol 6 simvoldan az ola bilməz' );
                $('#password_error').show();
            } else if(  value.length > 45){
                valid=false;
                $('#password_error').text('Parol 45 simvoldan çox ola bilməz' );
                $('#password_error').show();
            }
            if(valid){
                $('#password_error').text('');
                $('#password_error').hide();
            }
            return valid;
        }

        function  validatePasswordConfirm(){
            var PasswordConfirm=$('#password_confirm').val();
            var Password=$('#password').val();
            var valid = true;

            if (Password!=PasswordConfirm){
                valid= false;
                $('#passwordconfirm_error').text('Parollar eyni deyil');
                $('#passwordconfirm_error').show();
            }
            if(valid){
                $('#passwordconfirm_error').text('');
                $('#passwordconfirm_error').hide();
            }
            return valid;
        }

        $('#first_name').on('blur',function () {
            validateFirstName();
        });
        $('#last_name').on('blur',function () {
            validateLastName();
        });

        $('#email').on('blur',function () {
            validateEmail();
        });

        $('#password').on('blur',function () {
            validatePassword();
        });

        $('#password_confirm').on('blur', function () {
            validatePasswordConfirm();
        });

        $('#last_name').on('blur', function () {
            validateAlphaLast();
        });

        $('#first_name').on('blur', function () {
            validateAlphaFirst();
        });



    });
</script>



</body>

</html>
